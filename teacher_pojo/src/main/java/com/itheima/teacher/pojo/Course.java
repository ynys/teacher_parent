package com.itheima.teacher.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Course implements Serializable{

    private String courseId; // 课程主键
    private String courseName;// 课程名称
    private String courseDays;// 课程天数
    private String test; // 是否测试
    private String phaseCategoryId; // 课程阶段

}