package com.itheima.teacher.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Campus implements Serializable{

    private String campusId;
    private String campusName;
    private String campusHead;
    private String telephone;
}