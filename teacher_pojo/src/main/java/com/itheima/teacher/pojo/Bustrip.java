package com.itheima.teacher.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Bustrip implements Serializable{

    private String busTripId;
    private String campusId;
    private String userName;
    private String startTime;
    private String endTime;
    private String note;
    private String abc;
}