package com.itheima.teacher.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TeachingScope implements Serializable{

    private String teachingScopeId; // 授课范围主键
    private String userId;// 用户表ID(外键字段)
    private String teacherName;// 讲师姓名
    private String teachingScope; // 授课范围
    private int teachingDays; // 授课天数

}