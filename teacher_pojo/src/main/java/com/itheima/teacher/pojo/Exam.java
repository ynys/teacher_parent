package com.itheima.teacher.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

/**
 *  exam_id VARCHAR(50) PRIMARY KEY, #主键
 *  student_id VARCHAR(50) ,  #学生id
 *  dict_id VARCHAR(50) ,   #所处阶段
 *  classId varchar(50)
 *
 *  exam_score VARCHAR(50), #考试成绩
 *  lecturer VARCHAR(50) , #讲师
 *  assistant  VARCHAR(50) , #助教
 *
 *  class_teacher VARCHAR(50) , #班主任
 *  remarks VARCHAR(50) , #备注
 *  expand VARCHAR(50) # 扩展字段
 *
 *   scoreResultUrl1;
 *    scoreResultUrl2;
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Exam  implements Serializable {

    private String studentId;//学生id
    private String examScore;//考试成绩
    private String remarks;//备注

    private String classId;//班级id
    private String dictId;//字典id 所处阶段
    private String lecturer;//讲师

    private String assistant;//助教
    private String classTeacher;//班主任
    private String scoreResultUrl1;

    private String scoreResultUrl2;
    private String examId;//主键
    private String expand;//扩展字段

}
