package com.itheima.teacher.pojo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/*
 *   授课老师打分
 * */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClassRating implements Serializable {

    private String classRatingId;
    private String classId;
    private double ratingScore; // 分数
    private String teacher; // 老师
    private Date ratingTime; //评分时间
    private String courseStage; // 课程阶段

    private String className; // 班级名称

    @JSONField(format = "yyyy-MM-dd")
    public Date getRatingTime() {
        return ratingTime;
    }
}
