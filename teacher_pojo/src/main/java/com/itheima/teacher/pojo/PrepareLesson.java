package com.itheima.teacher.pojo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 备课计划
 */
@Data
@NoArgsConstructor
@ToString
public class PrepareLesson implements Serializable {

    private String id; // 主键ID
    private Date createTime; // 创建日期
    private Date updateTime; // 更新日期
    private String name; // 备课老师
    private String courseStage; // 课程节点
    private String courseInfo; // 课程信息
    private Date startDate; // 备课开始日期
    private Date endDate; // 备课截止日期
    private int courseDay; // 备课进度（天）
    private int isAccomplish; // 是否完成
    private String unAccomplish; // 未完成原因
    private int isUploadData; // 是否提交备课资料
    private String diskAddress; // 网盘地址
    private String remark; // 备注



    @JSONField(format = "yyyy-MM-dd")
    public Date getStartDate() {
        return startDate;
    }
    @JSONField(format = "yyyy-MM-dd")
    public Date getEndDate() {
        return endDate;
    }
}
