package com.itheima.teacher.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ClassHour implements Serializable{

    private String classHourId; // 课程主键
    private String scheduleMonth; // 月份
    private String teacher;// 讲师名称
    private double days1;// AB半天
    private double days2; // AB全天
    private double days3; // 传统全天
    private double totalHour; // 总天数

}