package com.itheima.teacher.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/*
 *   绩效实体
 * */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Performance implements Serializable {

    private String performanceId; // 绩效主键
    private String uId; // 用户id（外键字段）
    private Date createDate; // 创建日期
    private Date updateDate; // 更新日期
    private Double teachingTraditional; // 传统授课(传统模式)
    private Double teachingDay2; // AB全天
    private Double teachingDay1; // AB半天
    private Double teachingOther; // 就业指导/模拟面试/晚自习辅导
    private Double newProjectDevelopmentCode; // 新项目课程研发-项目（代码+资料）/项目（素材+案例）
    private Double newProjectDevelopmentNotes; // 新项目课程研发-课程讲义
    private Double newProjectDevelopmentTarget; // 新项目课程研发-课程学习目标
    private Double newProjectDevelopmentVideo; // 新项目课程研发-录制视频（不包含随堂视频）
    private Double oldProjectDevelopmentNotes; // 新非项目课程研发-课程讲义(代码+资料)
    private Double oldProjectDevelopmentTarget; // 新非项目课程研发-课程学习目标
    private Double oldProjectDevelopmentVideo; // 新非项目课程研发-录制视频（不包含随堂视频）
    private Double upgradeProjectCode; // 课程升级/项目课程升级-课程讲义(代码+资料)+学习目标
    private Double upgradeProjectVideo; // 课程升级/项目课程升级-录制视频（不包含随堂视频）
    private Double dualPpt; // 双元实施资料-双元实施PPT
    private Double dualNode; // 双元实施资料-双元实施讲义或笔记
    private Double dualVideo; // 双元实施资料-双元导师裁剪视频（随堂视频）
    private Double coverExamines; // 配套资料-阶段/升级考试（答案、批阅标准）
    private Double coverTraditional; // 配套资料-传统每日作业（视频+解题思路分析、答案）
    private Double coverAb; // 配套资料-AB全天每日作业（视频+解题思路分析、答案）
    private Double coverA; // 配套资料-AB半天每日作业（视频+解题思路分析、答案）
    private Double openClassTraining; // 精品微课（公开课）-在线实训/院校实训
    private Double openClassSpeak; // 精品微课（公开课）-公开课
    private Double openClassNewDevelopment; // 精品微课（公开课）-新课程研发
    private Double openClassOldDevelopment; // 精品微课（公开课）-已有课程研发
    private Double graduationProjectCode; // 毕业设计-课程讲义(代码+资料)
    private Double graduationProjectTarget; // 毕业设计-课程学习目标
    private Double graduationProjectVideo; // 毕业设计-录制视频（不包含随堂视频）
    private Double smallClassOnline; // 微课-在线微课
    private Double smallClassNewDevelopment; // 微课-新课程研发
    private Double smallClassOldDevelopment; // 微课-已有课程研发
    private Double teachingMaterialAudit; // 教材审核
    private Double bookVideo; // 视频录制

}
