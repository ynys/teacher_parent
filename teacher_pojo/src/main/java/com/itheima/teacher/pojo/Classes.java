package com.itheima.teacher.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/*
 * 班级实体
 * */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Classes implements Serializable {
    private String classId;
    private String className;
    private String classSize;
    private String classStartTime;
    private String classEndTime;

}
