package com.itheima.teacher.result;

import java.io.Serializable;

public enum ResutCode implements Serializable {

    SUCCESS(true, 10000, "操作成功"),
    FAIL(false, 20000, "操作失败"),
    ERROR(false, 99999, "服务器忙");

    // 操作是否成功
    private Boolean success;

    // 操作码
    private Integer code;

    // 提示信息
    private String message;

    private ResutCode(boolean success, int code, String message) {
        this.success = success;
        this.code = code;
        this.message = message;
    }

    public Boolean success() {
        return success;
    }

    public Integer code() {
        return code;
    }

    public String message() {
        return message;
    }
}
