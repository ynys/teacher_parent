package com.itheima.teacher.result;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class ResponseResult implements Serializable {

    // 操作是否成功
    private Boolean success;

    // 操作码
    private Integer code;

    // 提示信息
    private String message;

    public ResponseResult(ResutCode resutCode) {
        this.success = resutCode.success();
        this.code = resutCode.code();
        this.message = resutCode.message();
    }

    public ResponseResult(boolean success, int code, String message) {
        this.success = success;
        this.code = code;
        this.message = message;
    }

    public static ResponseResult SUCCESS() {
        return new ResponseResult(ResutCode.SUCCESS);
    }

    public static ResponseResult FAIL() {
        return new ResponseResult(ResutCode.FAIL);
    }

}
