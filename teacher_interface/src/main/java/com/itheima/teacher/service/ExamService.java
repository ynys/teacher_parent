package com.itheima.teacher.service;

;

import com.itheima.teacher.pojo.Exam;
import com.itheima.teacher.result.PageResult;
import com.itheima.teacher.result.ResponseResult;

import java.util.List;

public interface ExamService {
    PageResult findAll(int pageNumber, int pageSize);

    ResponseResult saveExam(Exam exam);

    Exam updateExamUI(String id);

    ResponseResult updateExam(Exam exam);

    ResponseResult deleteExamById(String id);

    void saveExamList(List<Object> objects);

    List<List<String>> findByCondition();

}
