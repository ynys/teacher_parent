package com.itheima.teacher.service;

import com.itheima.teacher.pojo.Bustrip;
import com.itheima.teacher.result.PageResult;
import com.itheima.teacher.result.ResponseResult;

import java.util.List;

public interface BustripService {

    // 分页查询的方法
    public PageResult findByPage(Integer pageNum, Integer pageSize);

    // 添加
    public ResponseResult insert(Bustrip bustrip);

    // 修改
    public ResponseResult update(Bustrip bustrip);

    // 删除
    public ResponseResult delete(Integer id);

    // 查询一个
    public Bustrip findOne(Integer id);

    ResponseResult dels(Integer[] ids);

    List<Bustrip> findAll();
}
