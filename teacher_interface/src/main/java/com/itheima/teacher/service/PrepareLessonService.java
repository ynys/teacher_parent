package com.itheima.teacher.service;

import com.itheima.teacher.pojo.PrepareLesson;
import com.itheima.teacher.result.PageResult;
import com.itheima.teacher.result.ResponseResult;
// 备课计划服务接口
public interface PrepareLessonService {

    public PageResult findByPage(Integer pageNum, Integer pageSize);

    public ResponseResult update(PrepareLesson prepareLesson);

    public ResponseResult insert(PrepareLesson prepareLesson);

    public ResponseResult delete(String id);

    public PrepareLesson findOne(String id);

    ResponseResult dels(String[] ids);
}
