package com.itheima.teacher.service;

import com.itheima.teacher.pojo.Campus;
import com.itheima.teacher.result.PageResult;
import com.itheima.teacher.result.ResponseResult;

import java.util.List;

public interface CampusService {

    // 分页查询的方法
    public PageResult findByPage(Integer pageNum, Integer pageSize);

    // 添加
    public ResponseResult insert(Campus campus);

    // 修改
    public ResponseResult update(Campus campus);

    // 删除
    public ResponseResult delete(Integer id);

    // 查询一个
    public Campus findOne(Integer id);

    ResponseResult dels(Integer[] ids);

    List<Campus> findAll();
}
