package com.itheima.teacher.service;


import com.itheima.teacher.pojo.Course;
import com.itheima.teacher.result.PageResult;
import com.itheima.teacher.result.ResponseResult;

public interface CourseService {
    public PageResult findByPage(Integer pageNum, Integer pageSize);

    public ResponseResult update(Course course);

    public ResponseResult insert(Course course);

    public ResponseResult delete(String id);

    public Course findOne(String id);
}
