package com.itheima.teacher.service;

import com.itheima.teacher.pojo.ClassRating;
import com.itheima.teacher.result.PageResult;
import com.itheima.teacher.result.ResponseResult;


// 评分信息服务接口
public interface ClassRatingService {
    public PageResult findByPage(Integer pageNum, Integer pageSize);

    public ResponseResult update(ClassRating classRating);

    public ResponseResult insert(ClassRating classRating);

    public ResponseResult delete(String classRatingId);

    public ClassRating findOne(String classRatingId);

    ResponseResult dels(String[] ids);
}
