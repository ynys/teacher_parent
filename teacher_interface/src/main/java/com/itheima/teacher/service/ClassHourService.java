package com.itheima.teacher.service;


import com.itheima.teacher.pojo.ClassHour;
import com.itheima.teacher.result.PageResult;
import com.itheima.teacher.result.ResponseResult;

public interface ClassHourService {
    public PageResult findByPage(Integer pageNum, Integer pageSize);

    public ResponseResult update(ClassHour classHour);

    public ResponseResult insert(ClassHour classHour);

    public ResponseResult delete(String id);

    public ClassHour findOne(String id);
}
