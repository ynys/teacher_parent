package com.itheima.teacher.service;

import com.itheima.teacher.pojo.Classes;
import com.itheima.teacher.result.PageResult;

import java.util.List;

// 班级信息服务接口
public interface ClassesService {


    public List<Classes> findAll();


    public PageResult findByPage(Integer pageNum, Integer pageSize);

    public Classes findOne(String classId);
}
