package com.itheima.teacher.service;


import com.itheima.teacher.pojo.Course;
import com.itheima.teacher.pojo.TeachingScope;
import com.itheima.teacher.result.PageResult;
import com.itheima.teacher.result.ResponseResult;

public interface TeachingScopeService {
    public PageResult findByPage(Integer pageNum, Integer pageSize);

    public ResponseResult update(TeachingScope teachingScope);

    public ResponseResult insert(TeachingScope teachingScope);

    public ResponseResult delete(String id);

    public TeachingScope findOne(String id);
}
