package com.itheima.teacher.service;

import com.itheima.teacher.pojo.Performance;
import com.itheima.teacher.result.PageResult;

// 绩效信息服务接口
public interface PerformanceService {

    // 分页查询的方法
    public PageResult findByPage(Integer pageNum, Integer pageSize);

    public Performance findOne(String performanceId);
}
