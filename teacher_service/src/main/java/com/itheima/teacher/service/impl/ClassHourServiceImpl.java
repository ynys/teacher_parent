package com.itheima.teacher.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.teacher.dao.ClassHourDao;
import com.itheima.teacher.pojo.ClassHour;
import com.itheima.teacher.result.PageResult;
import com.itheima.teacher.result.ResponseResult;
import com.itheima.teacher.service.ClassHourService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ClassHourServiceImpl implements ClassHourService {

    @Autowired
    private ClassHourDao dao;


    @Override
    public PageResult findByPage(Integer pageNum, Integer pageSize) {
        // 使用第三方分页插件
        PageHelper.startPage(pageNum, pageSize);
        // 调用dao实现查询
        Page<ClassHour> page = (Page<ClassHour>) dao.findAll();
        // 获取总记录数
        long total = page.getTotal();
        // 每页list集合
        List<ClassHour> list = page.getResult();
        return new PageResult(total, list);
    }

    @Override
    public ResponseResult update(ClassHour classHour) {
        int result = dao.update(classHour);
        if (result > 0) {
            return ResponseResult.SUCCESS();
        } else {

            return ResponseResult.FAIL();
        }

    }

    @Override
    public ResponseResult insert(ClassHour classHour) {
        int result = dao.insert(classHour);
        if (result > 0) {
            return ResponseResult.SUCCESS();
        } else {

            return ResponseResult.FAIL();
        }

    }

    @Override
    public ResponseResult delete(String id) {
        int result = dao.delete(id);
        if (result > 0) {
            return ResponseResult.SUCCESS();
        } else {

            return ResponseResult.FAIL();
        }
    }

    @Override
    public ClassHour findOne(String id) {
        return dao.findOne(id);
    }
}
