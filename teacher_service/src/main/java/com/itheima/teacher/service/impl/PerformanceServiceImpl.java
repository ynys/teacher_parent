package com.itheima.teacher.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itheima.teacher.dao.PerformanceDao;
import com.itheima.teacher.pojo.Performance;
import com.itheima.teacher.result.PageResult;
import com.itheima.teacher.service.PerformanceService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service
public class PerformanceServiceImpl implements PerformanceService {

    @Autowired
    PerformanceDao performanceDao;

    @Override
    public PageResult findByPage(Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Performance> list = performanceDao.findAll();
        PageInfo<Performance> pageInfo = new PageInfo<>(list);
        return new PageResult(pageInfo.getTotal(), pageInfo.getList());
    }

    @Override
    public Performance findOne(String performanceId) {
        return performanceDao.findOne(performanceId);
    }
}
