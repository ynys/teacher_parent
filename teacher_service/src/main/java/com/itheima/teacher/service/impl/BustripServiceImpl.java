package com.itheima.teacher.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.teacher.dao.BustripDao;
import com.itheima.teacher.pojo.Bustrip;
import com.itheima.teacher.result.PageResult;
import com.itheima.teacher.result.ResponseResult;
import com.itheima.teacher.service.BustripService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
@Transactional
public class BustripServiceImpl implements BustripService {

    @Autowired
    private BustripDao bustripDao;

    @Override
    public PageResult findByPage(Integer pageNum, Integer pageSize) {
        // 使用第三方分页插件
        PageHelper.startPage(pageNum, pageSize);
        // 调用dao实现查询
        Page<Bustrip> page = (Page<Bustrip>) bustripDao.findAll();
        // 获取总记录数
        long total = page.getTotal();
        // 每页list集合
        List<Bustrip> list = page.getResult();
        return new PageResult(total, list);
    }

    @Override
    public ResponseResult insert(Bustrip bustrip) {
        bustrip.setBusTripId(UUID.randomUUID().toString());
        int result = bustripDao.insert(bustrip);

        if (result > 0) {
            return ResponseResult.SUCCESS();
        } else {

            return ResponseResult.FAIL();
        }
    }

    @Override
    public ResponseResult update(Bustrip bustrip) {

        int result = bustripDao.update(bustrip);

        if (result > 0) {
            return ResponseResult.SUCCESS();
        } else {

            return ResponseResult.FAIL();
        }
    }

    @Override
    public ResponseResult delete(Integer id) {
        int result = bustripDao.delete(id);
        if (result > 0) {
            return ResponseResult.SUCCESS();
        } else {

            return ResponseResult.FAIL();
        }
    }

    @Override
    public Bustrip findOne(Integer id) {
        return bustripDao.findOne(id);
    }

    @Override
    public ResponseResult dels(Integer[] ids) {
        if (ids != null && ids.length > 0) {
            for (Integer id : ids) {
                bustripDao.delete(id);
            }
           return ResponseResult.SUCCESS();
        }
        return ResponseResult.FAIL();
    }

    @Override
    public List<Bustrip> findAll() {
        return bustripDao.findAll();
    }
}
