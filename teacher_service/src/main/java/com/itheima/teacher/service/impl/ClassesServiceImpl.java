package com.itheima.teacher.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itheima.teacher.dao.ClassesDao;
import com.itheima.teacher.pojo.Classes;
import com.itheima.teacher.result.PageResult;
import com.itheima.teacher.service.ClassesService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service
public class ClassesServiceImpl implements ClassesService {

    @Autowired
    ClassesDao classesDao;

    @Override
    public List<Classes> findAll() {
        return classesDao.findAll();
    }

    @Override
    public PageResult findByPage(Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Classes> list = classesDao.findAll();
        PageInfo<Classes> pageInfo = new PageInfo<>(list);
        return new PageResult(pageInfo.getTotal(), pageInfo.getList());
    }

    @Override
    public Classes findOne(String classId) {
        return classesDao.findOne(classId);
    }
}
