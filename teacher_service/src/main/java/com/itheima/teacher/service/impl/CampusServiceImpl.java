package com.itheima.teacher.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.teacher.dao.CampusDao;
import com.itheima.teacher.pojo.Campus;
import com.itheima.teacher.result.PageResult;
import com.itheima.teacher.result.ResponseResult;
import com.itheima.teacher.service.CampusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
@Transactional
public class CampusServiceImpl implements CampusService {

    @Autowired
    private CampusDao campusDao;

    @Override
    public PageResult findByPage(Integer pageNum, Integer pageSize) {
        // 使用第三方分页插件
        PageHelper.startPage(pageNum, pageSize);
        // 调用dao实现查询
        Page<Campus> page = (Page<Campus>) campusDao.findAll();
        // 获取总记录数
        long total = page.getTotal();
        // 每页list集合
        List<Campus> list = page.getResult();
        return new PageResult(total, list);
    }

    @Override
    public ResponseResult insert(Campus campus) {
        campus.setCampusId(UUID.randomUUID().toString());
        int result = campusDao.insert(campus);

        if (result > 0) {
            return ResponseResult.SUCCESS();
        } else {

            return ResponseResult.FAIL();
        }
    }

    @Override
    public ResponseResult update(Campus campus) {

        int result = campusDao.update(campus);

        if (result > 0) {
            return ResponseResult.SUCCESS();
        } else {

            return ResponseResult.FAIL();
        }
    }

    @Override
    public ResponseResult delete(Integer id) {
        int result = campusDao.delete(id);
        if (result > 0) {
            return ResponseResult.SUCCESS();
        } else {

            return ResponseResult.FAIL();
        }
    }

    @Override
    public Campus findOne(Integer id) {
        return campusDao.findOne(id);
    }

    @Override
    public ResponseResult dels(Integer[] ids) {
        if (ids != null && ids.length > 0) {
            for (Integer id : ids) {
                campusDao.delete(id);
            }
           return ResponseResult.SUCCESS();
        }
        return ResponseResult.FAIL();
    }

    @Override
    public List<Campus> findAll() {
        return campusDao.findAll();
    }
}
