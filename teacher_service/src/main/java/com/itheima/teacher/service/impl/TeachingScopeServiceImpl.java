package com.itheima.teacher.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.teacher.dao.ClassHourDao;
import com.itheima.teacher.dao.TeachingScopeDao;
import com.itheima.teacher.pojo.TeachingScope;
import com.itheima.teacher.result.PageResult;
import com.itheima.teacher.result.ResponseResult;
import com.itheima.teacher.service.ClassHourService;
import com.itheima.teacher.service.TeachingScopeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class TeachingScopeServiceImpl implements TeachingScopeService {

    @Autowired
    private TeachingScopeDao dao;


    @Override
    public PageResult findByPage(Integer pageNum, Integer pageSize) {
        // 使用第三方分页插件
        PageHelper.startPage(pageNum, pageSize);
        // 调用dao实现查询
        Page<TeachingScope> page = (Page<TeachingScope>) dao.findAll();
        // 获取总记录数
        long total = page.getTotal();
        // 每页list集合
        List<TeachingScope> list = page.getResult();
        return new PageResult(total, list);
    }

    @Override
    public ResponseResult update(TeachingScope teachingScope) {
        int result = dao.update(teachingScope);
        if (result > 0) {
            return ResponseResult.SUCCESS();
        } else {

            return ResponseResult.FAIL();
        }

    }

    @Override
    public ResponseResult insert(TeachingScope teachingScope) {
        int result = dao.insert(teachingScope);
        if (result > 0) {
            return ResponseResult.SUCCESS();
        } else {

            return ResponseResult.FAIL();
        }

    }

    @Override
    public ResponseResult delete(String id) {
        int result = dao.delete(id);
        if (result > 0) {
            return ResponseResult.SUCCESS();
        } else {

            return ResponseResult.FAIL();
        }
    }

    @Override
    public TeachingScope findOne(String id) {
        return dao.findOne(id);
    }
}
