package com.itheima.teacher.service.impl;

import com.alibaba.druid.sql.visitor.functions.If;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itheima.teacher.dao.PrepareLessonDao;
import com.itheima.teacher.pojo.PrepareLesson;
import com.itheima.teacher.result.PageResult;
import com.itheima.teacher.result.ResponseResult;
import com.itheima.teacher.service.PrepareLessonService;
import com.itheima.teacher.utils.UuidUtil;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

@Service
public class PrepareLessonServiceImpl implements PrepareLessonService {

    @Autowired
    private PrepareLessonDao prepareLessonDao;

    // 分页查询
    @Override
    public PageResult findByPage(Integer pageNum, Integer pageSize) {
        // 计算分页
        PageHelper.startPage(pageNum, pageSize);
        // 调用dao查询
        List<PrepareLesson> list = prepareLessonDao.findAll();
        // 转为分页对象
        PageInfo<PrepareLesson> pageInfo = new PageInfo<>(list);
        // 返回分页对象
        return new PageResult(pageInfo.getTotal(), pageInfo.getList());
    }

    @Override
    public ResponseResult update(PrepareLesson prepareLesson) {

        prepareLesson.setUpdateTime(new Date());
        int result = prepareLessonDao.update(prepareLesson);

        if (result > 0) {
            return ResponseResult.SUCCESS();
        } else {

            return ResponseResult.FAIL();
        }
    }

    @Override
    public ResponseResult insert(PrepareLesson prepareLesson) {
        prepareLesson.setId(UuidUtil.getUuid());
        prepareLesson.setCreateTime(new Date());
        int result = prepareLessonDao.insert(prepareLesson);

        if (result > 0) {
            return ResponseResult.SUCCESS();
        } else {

            return ResponseResult.FAIL();
        }
    }

    @Override
    public ResponseResult delete(String id) {
        int result = prepareLessonDao.delete(id);
        if (result > 0) {
            return ResponseResult.SUCCESS();
        } else {

            return ResponseResult.FAIL();
        }
    }

    @Override
    public PrepareLesson findOne(String id) {
        return prepareLessonDao.findOne(id);
    }

    @Override
    public ResponseResult dels(String[] ids) {
        if (ids.length > 0) {
            for (String id : ids) {
                int result = prepareLessonDao.delete(id);
            }
            return ResponseResult.SUCCESS();
        }
        return ResponseResult.FAIL();
    }
}
