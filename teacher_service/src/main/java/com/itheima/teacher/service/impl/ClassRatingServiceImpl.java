package com.itheima.teacher.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itheima.teacher.dao.ClassRatingDao;
import com.itheima.teacher.pojo.ClassRating;
import com.itheima.teacher.result.PageResult;
import com.itheima.teacher.result.ResponseResult;
import com.itheima.teacher.service.ClassRatingService;
import com.itheima.teacher.utils.UuidUtil;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service
public class ClassRatingServiceImpl implements ClassRatingService {

    @Autowired
    private ClassRatingDao classRatingDao;

    // 分页查询
    @Override
    public PageResult findByPage(Integer pageNum, Integer pageSize) {
        // 计算分页
        PageHelper.startPage(pageNum, pageSize);
        // 调用dao查询
        List<ClassRating> list = classRatingDao.findAll();
        // 转为分页对象
        PageInfo<ClassRating> pageInfo = new PageInfo<>(list);
        // 返回分页对象
        return new PageResult(pageInfo.getTotal(), pageInfo.getList());
    }

    @Override
    public ResponseResult update(ClassRating classRating) {

        int result = classRatingDao.update(classRating);

        if (result > 0) {
            return ResponseResult.SUCCESS();
        } else {

            return ResponseResult.FAIL();
        }
    }

    @Override
    public ResponseResult insert(ClassRating classRating) {
        classRating.setClassRatingId(UuidUtil.getUuid());
        int result = classRatingDao.insert(classRating);

        if (result > 0) {
            return ResponseResult.SUCCESS();
        } else {

            return ResponseResult.FAIL();
        }
    }

    @Override
    public ResponseResult delete(String classRatingId) {
        int result = classRatingDao.delete(classRatingId);
        if (result > 0) {
            return ResponseResult.SUCCESS();
        } else {

            return ResponseResult.FAIL();
        }
    }

    @Override
    public ClassRating findOne(String classRatingId) {
        return classRatingDao.findOne(classRatingId);
    }

    @Override
    public ResponseResult dels(String[] ids) {
        if (ids.length > 0) {
            for (String id : ids) {
                int result = classRatingDao.delete(id);
            }
            return ResponseResult.SUCCESS();
        }
        return ResponseResult.FAIL();
    }
}
