package com.itheima.teacher.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.teacher.dao.CourseDao;
import com.itheima.teacher.pojo.Course;
import com.itheima.teacher.result.PageResult;
import com.itheima.teacher.result.ResponseResult;
import com.itheima.teacher.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CourseServiceImpl implements CourseService {

    @Autowired
    private CourseDao courseDao;


    @Override
    public PageResult findByPage(Integer pageNum, Integer pageSize) {
        // 使用第三方分页插件
        PageHelper.startPage(pageNum, pageSize);
        // 调用dao实现查询
        Page<Course> page = (Page<Course>) courseDao.findAll();
        // 获取总记录数
        long total = page.getTotal();
        // 每页list集合
        List<Course> list = page.getResult();
        return new PageResult(total, list);
    }

    @Override
    public ResponseResult update(Course course) {
        int result = courseDao.update(course);
        if (result > 0) {
            return ResponseResult.SUCCESS();
        } else {

            return ResponseResult.FAIL();
        }

    }

    @Override
    public ResponseResult insert(Course course) {
        int result = courseDao.insert(course);
        if (result > 0) {
            return ResponseResult.SUCCESS();
        } else {

            return ResponseResult.FAIL();
        }

    }

    @Override
    public ResponseResult delete(String id) {
        int result = courseDao.delete(id);
        if (result > 0) {
            return ResponseResult.SUCCESS();
        } else {

            return ResponseResult.FAIL();
        }
    }

    @Override
    public Course findOne(String id) {
        return courseDao.findOne(id);
    }
}
