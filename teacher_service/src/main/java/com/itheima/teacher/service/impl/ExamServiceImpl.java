package com.itheima.teacher.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.teacher.dao.ExamDao;
import com.itheima.teacher.pojo.Exam;
import com.itheima.teacher.result.PageResult;
import com.itheima.teacher.result.ResponseResult;
import com.itheima.teacher.service.ExamService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.UUID;

@Service
public class ExamServiceImpl implements ExamService {
    @Autowired
    private ExamDao examDao ;
    @Override
    public PageResult findAll(int pageNumber, int pageSize) {
        PageHelper.startPage(pageNumber , pageSize);
        Page<Exam> page = (Page<Exam>)examDao.findAll();

        long total = page.getTotal();
        List<Exam> exams = page.getResult();
        return new PageResult(total , exams);
    }

    @Override
    public ResponseResult saveExam(Exam exam) {
        int count = examDao.saveExam(exam);
        if(count>0){
            return ResponseResult.SUCCESS();
        }
        return ResponseResult.FAIL();
    }

    @Override
    public Exam updateExamUI(String id) {
        return  examDao.updateExamUI(id);
    }

    @Override
    public ResponseResult updateExam(Exam exam) {
        int count = examDao.updateExam(exam);
        if(count>0){
            return ResponseResult.SUCCESS();
        }
        return ResponseResult.FAIL();
    }

    @Override
    public ResponseResult deleteExamById(String id) {
        int count = examDao.deleteExamById(id);
        if(count>0){
            return ResponseResult.SUCCESS();
        }
        return ResponseResult.FAIL();

    }

    /**
     * 上传
     * @param objects
     */
    @Override
    public void saveExamList(List<Object> objects) {
        for (Object object : objects) {
            Exam exam = (Exam)object;
            exam.setExamId(UUID.randomUUID().toString().replaceAll("-","").toUpperCase());
            examDao.saveExam(exam);
        }
    }

    @Override
    public List<List<String>> findByCondition() {
        return examDao.findByCondition();
    }
}
