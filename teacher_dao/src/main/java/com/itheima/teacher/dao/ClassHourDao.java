package com.itheima.teacher.dao;

import com.itheima.teacher.pojo.ClassHour;
import com.itheima.teacher.pojo.Course;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface ClassHourDao {
    @Select("SELECT * from `class_hour`")
    public List<ClassHour> findAll();

    @Update("update class_hour set schedule_month=#{scheduleMonth},teacher=#{teacher},days1=#{days1},days2=#{days2},days3=#{days3},total_hour=#{totalHour} where class_hour_id = #{classHourId}")
    public int update(ClassHour classHour);

    @Insert("insert into class_hour values(#{classHourId},#{scheduleMonth},#{teacher},#{days1},#{days2},#{days3},#{totalHour})")
    public int insert(ClassHour classHour);

    @Delete("delete from class_hour where class_hour_id = #{classHourId}")
    public int delete(String id);

    @Select("SELECT * from `class_hour` WHERE class_hour_id = #{classHourId}")
    public ClassHour findOne(String id);
}
