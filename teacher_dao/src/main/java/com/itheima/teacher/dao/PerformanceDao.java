package com.itheima.teacher.dao;

import com.itheima.teacher.pojo.Performance;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface PerformanceDao {

    @Select("select * from performance order by create_date desc")
    public List<Performance> findAll();

    @Select("select * from performance where performance_id = #{performanceId}")
    public Performance findOne(String performanceId);
}
