package com.itheima.teacher.dao;

import com.itheima.teacher.pojo.Course;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface CourseDao {
    @Select("SELECT * from `course`")
    public List<Course> findAll();

    @Update("update course set course_name=#{courseName},course_days=#{courseDays},test=#{test},phase_category_id=#{phaseCategoryId} where course_id = #{courseId}")
    public int update(Course course);

    @Insert("insert into course values(#{courseId},#{courseName},#{courseDays},#{test},#{phaseCategoryId})")
    public int insert(Course course);

    @Delete("delete from course where course_id = #{courseId}")
    public int delete(String id);

    @Select("SELECT * from `course` WHERE course_id = #{course}")
    public Course findOne(String id);
}
