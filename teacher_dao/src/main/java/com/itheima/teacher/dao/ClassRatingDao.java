package com.itheima.teacher.dao;

import com.itheima.teacher.pojo.ClassRating;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface ClassRatingDao {


    @Select("SELECT * FROM  class_rating cr ,classes c WHERE cr.`class_id`=c.`class_id` ORDER BY cr.`ratingTime` DESC")
    public List<ClassRating> findAll();

    @Update("update class_rating set class_id=#{classId},ratingScore=#{ratingScore},teacher=#{teacher},ratingTime=#{ratingTime},course_stage=#{courseStage}  where class_rating_id = #{classRatingId}")
    public int update(ClassRating classRating);

    @Insert("insert into class_rating values(#{classRatingId},#{classId},#{ratingScore},#{teacher},#{ratingTime},#{courseStage})")
    public int insert(ClassRating classRating);

    @Delete("delete from class_rating where class_rating_id = #{classRatingId}")
    public int delete(String classRatingId);

    @Select("SELECT * from class_rating WHERE class_rating_id = #{classRatingId}")
    public ClassRating findOne(String classRatingId);
}
