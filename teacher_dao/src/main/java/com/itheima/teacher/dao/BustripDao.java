package com.itheima.teacher.dao;

import com.itheima.teacher.pojo.Bustrip;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface BustripDao {

    @Select("SELECT * from `business_trip`")
    public List<Bustrip> findAll();

    @Update("update business_trip set campus_id=#{campusId},user_name=#{userName},start_time=#{startTime},end_time=#{endTime},note=#{note},abc=#{abc} where business_trip_id = #{busTripId}")
    public int update(Bustrip business_trip);

    @Insert("insert into business_trip values(#{busTripId},#{campusId},#{userName},#{startTime},#{endTime},#{note},#{abc})")
    public int insert(Bustrip business_trip);

    @Delete("delete from business_trip where business_trip_id = #{busTripId}")
    public int delete(Integer id);

    @Select("SELECT * from `business_trip` WHERE business_trip_id = #{busTripId}")
    public Bustrip findOne(Integer id);
}
