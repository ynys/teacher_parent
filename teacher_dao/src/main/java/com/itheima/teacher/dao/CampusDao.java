package com.itheima.teacher.dao;

import com.itheima.teacher.pojo.Campus;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface CampusDao {

    @Select("SELECT * from `campus`")
    public List<Campus> findAll();

    @Update("update campus set campus_name=#{campusName},campus_head=#{campusHead},telephone=#{telephone} where campus_id = #{campusId}")
    public int update(Campus campus);

    @Insert("insert into campus values(#{campusId},#{campusName},#{campusHead},#{telephone})")
    public int insert(Campus campus);

    @Delete("delete from campus where campus_id = #{campusId}")
    public int delete(Integer id);

    @Select("SELECT * from `campus` WHERE campus_id = #{campusId}")
    public Campus findOne(Integer id);
}
