package com.itheima.teacher.dao;

import com.itheima.teacher.pojo.PrepareLesson;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * 备课计划dao
 */
public interface PrepareLessonDao {

    @Select("SELECT * FROM  prepare_lesson order by create_time")
    public List<PrepareLesson> findAll();

    @Update("update prepare_lesson set create_time=#{createTime},update_time=#{updateTime},name=#{name},course_stage=#{courseStage},course_info=#{courseInfo},start_date=#{startDate},end_date=#{endDate},course_day=#{courseDay},is_accomplish=#{isAccomplish},un_accomplish=#{unAccomplish},is_upload_data=#{isUploadData},disk_address=#{diskAddress},remark=#{remark} where id = #{id}")
    public int update(PrepareLesson prepareLesson);

    @Insert("insert into prepare_lesson values(#{id},#{createTime},#{updateTime},#{name},#{courseStage},#{courseInfo},#{startDate},#{endDate},#{courseDay},#{isAccomplish},#{unAccomplish},#{isUploadData},#{diskAddress},#{remark})")
    public int insert(PrepareLesson prepareLesson);

    @Delete("delete from prepare_lesson where id = #{id}")
    public int delete(String id);

    @Select("SELECT * from prepare_lesson WHERE id = #{id}")
    public PrepareLesson findOne(String id);
}
