package com.itheima.teacher.dao;


import com.itheima.teacher.pojo.Exam;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface ExamDao {

    @Select("select * from tbl_exam")
    @Results(value ={
            @Result(id = true , property = "examId" , column = "exam_Id") ,
            @Result(property =  "studentId" , column = "student_Id"),
            @Result(property =  "dictId" , column = "dict_Id"),
            @Result(property =  "classId" , column = "class_Id"),
            @Result(property =  "examScore" , column = "exam_Score"),
            @Result(property =  "lecturer" , column = "lecturer"),
            @Result(property =  "assistant" , column = "assistant"),
            @Result(property =  "classTeacher" , column = "class_Teacher"),
            @Result(property =  "remarks" , column = "remarks"),
            @Result(property =  "expand" , column = "expand"),
            @Result(property =  "scoreResultUrl1" , column = "score_Result_Url_1"),
            @Result(property =  "scoreResultUrl2" , column = "score_Result_Url_2")
    })
    public List<Exam> findAll();

    @Insert("INSERT INTO `tbl_exam`VALUES( #{examId},#{studentId},#{dictId},#{classId},#{examScore},#{lecturer}, #{assistant},#{classTeacher},#{remarks},#{expand},#{scoreResultUrl1},#{scoreResultUrl2}) ")
    int saveExam(Exam exam);

    @Select("select * from tbl_exam where exam_id=#{id}")
    @Results(value ={
            @Result(id = true , property = "examId" , column = "exam_Id") ,
            @Result(property =  "studentId" , column = "student_Id"),
            @Result(property =  "dictId" , column = "dict_Id"),
            @Result(property =  "classId" , column = "class_Id"),
            @Result(property =  "examScore" , column = "exam_Score"),
            @Result(property =  "lecturer" , column = "lecturer"),
            @Result(property =  "assistant" , column = "assistant"),
            @Result(property =  "classTeacher" , column = "class_Teacher"),
            @Result(property =  "remarks" , column = "remarks"),
            @Result(property =  "expand" , column = "expand"),
            @Result(property =  "scoreResultUrl1" , column = "score_Result_Url_1"),
            @Result(property =  "scoreResultUrl2" , column = "score_Result_Url_2")
    })
    Exam updateExamUI(String id);


    @Update("UPDATE `tbl_exam` SET `exam_id` = #{examId},`student_id` = #{studentId},`dict_id` = #{dictId},`class_Id`=#{classId},`exam_score` = #{examScore},`lecturer` = #{lecturer},`assistant` = #{assistant},`class_teacher` =#{classTeacher}, `remarks` = #{remarks},`expand` = #{expand},`score_Result_Url_1`=#{scoreResultUrl1},`score_Result_Url_2`=#{scoreResultUrl2} WHERE `exam_id` = #{examId}")
    int updateExam(Exam exam);

    @Delete("delete from tbl_exam where exam_id=#{id}")
    int deleteExamById(String id);

    @Select("select * from tbl_exam")
    List<List<String>> findByCondition();



    //void myBean();
}
