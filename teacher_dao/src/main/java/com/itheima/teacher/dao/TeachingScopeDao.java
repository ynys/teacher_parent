package com.itheima.teacher.dao;

import com.itheima.teacher.pojo.TeachingScope;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface TeachingScopeDao {
    @Select("SELECT * from `teaching_scope`")
    public List<TeachingScope> findAll();

    @Update("update teaching_scope set user_id=#{userId},teacher_name=#{teacherName},teaching_scope=#{teachingScope},teaching_days=#{teachingDays} where teaching_scope_id = #{teachingScopeId}")
    public int update(TeachingScope teaching_scope);

    @Insert("insert into teaching_scope values(#{teachingScopeId},#{userId},#{teacherName},#{teachingScope},#{teachingDays})")
    public int insert(TeachingScope teaching_scope);

    @Delete("delete from teaching_scope where teaching_scope_id = #{teachingScopeId}")
    public int delete(String teaching_scope_id);

    @Select("SELECT * from `teaching_scope` WHERE teaching_scope_id = #{teachingScopeId}")
    public TeachingScope findOne(String teaching_scope_id);
}
