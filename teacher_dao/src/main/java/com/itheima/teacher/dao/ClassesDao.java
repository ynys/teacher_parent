package com.itheima.teacher.dao;

import com.itheima.teacher.pojo.Classes;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface ClassesDao {

    @Select("SELECT * FROM classes ORDER BY class_name")
    public List<Classes> findAll();

    @Select("SELECT * FROM classes where class_id = #{classId}")
    public Classes findOne(String classId);
}
