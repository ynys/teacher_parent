package com.itheima.teacher.exception;

import com.itheima.teacher.result.ResponseResult;
import com.itheima.teacher.result.ResutCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/*
    全局异常处理器
* */
@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    // 未知异常
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ResponseResult handleExeception(Exception e) {
        log.error(e.getMessage(), e);
        return new ResponseResult(ResutCode.ERROR);
    }


    // 自定义异常
    @ExceptionHandler(BusinessException.class)
    @ResponseBody
    public ResponseResult handleBusinessException(BusinessException e) {
        log.error(e.getMessage(), e);
        return new ResponseResult(false, 9999, e.getMessage());
    }
}
