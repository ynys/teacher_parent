package com.itheima.teacher.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.teacher.pojo.Classes;
import com.itheima.teacher.service.ClassesService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 班级
 */
@RestController
@RequestMapping("/classes/")
public class ClassesController {

    @Reference
    ClassesService classesService;

    @GetMapping("/find_all")
    public List<Classes> findAll() {
        return classesService.findAll();
    }

    @GetMapping("/find_one/{id}")
    public Classes findOne(@PathVariable String id) {
        return classesService.findOne(id);
    }
}
