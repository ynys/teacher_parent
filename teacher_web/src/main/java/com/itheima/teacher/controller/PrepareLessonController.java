package com.itheima.teacher.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.teacher.pojo.PrepareLesson;
import com.itheima.teacher.result.PageResult;
import com.itheima.teacher.result.ResponseResult;
import com.itheima.teacher.service.PrepareLessonService;
import org.springframework.web.bind.annotation.*;

/**
 * 备课计划
 */
@RestController
@RequestMapping("/prepare_lesson/")
public class PrepareLessonController {

    @Reference
    private PrepareLessonService prepareLessonService;

    @GetMapping("/find_page")
    public PageResult findByPage(Integer pageNum, Integer pageSize) {

        return prepareLessonService.findByPage(pageNum, pageSize);
    }


    // 添加
    @PostMapping("/insert")
    public ResponseResult insert(@RequestBody PrepareLesson prepareLesson) {
        return prepareLessonService.insert(prepareLesson);
    }

    // 修改
    @PutMapping("/update")
    public ResponseResult update(@RequestBody PrepareLesson prepareLesson) {
        return prepareLessonService.update(prepareLesson);
    }

    // 删除
    @DeleteMapping("/delete/{id}")
    public ResponseResult delete(@PathVariable String id) {
        return prepareLessonService.delete(id);
    }

    // 查询一个
    @GetMapping("/find_one/{id}")
    public PrepareLesson findOne(@PathVariable String id) {
        return prepareLessonService.findOne(id);
    }

    // 批量删除
    @DeleteMapping("/dels")
    public ResponseResult dels(String[] ids){return prepareLessonService.dels(ids);}
}
