package com.itheima.teacher.controller;

import com.alibaba.dubbo.config.annotation.Reference;

import com.itheima.teacher.pojo.Exam;
import com.itheima.teacher.result.PageResult;
import com.itheima.teacher.result.ResponseResult;
import com.itheima.teacher.service.ExamService;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/exam")
public class ExamController {
    /**
     * 查询
     */
    @Reference
    private ExamService examService;
    @GetMapping("/findAll")
    public PageResult findAll(int pageNumber , int  pageSize){
        System.out.println(pageNumber + "@@" + pageSize);
        System.out.println(examService.findAll(pageNumber , pageSize));
        return examService.findAll(pageNumber , pageSize);
    }


    /**
     * 保存
     * @param exam
     * @return
     */
    @PostMapping("/saveExam")
    public ResponseResult saveExam(@RequestBody Exam exam){
        exam.setExamId(UUID.randomUUID().toString().replace("-", "").toUpperCase());
        return examService.saveExam(exam);
    }

    /**
     * 根据id查询
     * @param id
     * @return
     */
    @GetMapping("/updateExamUI/{id}")
    public Exam updateExamUI(@PathVariable String id){
        System.out.println( examService.updateExamUI(id));
        return  examService.updateExamUI(id);
    }

    /**
     * 修改
     * @param exam
     * @return
     */
    @PutMapping("/updateExam")
    public ResponseResult updateExam(@RequestBody Exam exam){

        return examService.updateExam(exam);
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @DeleteMapping("/deleteByEid/{id}")
    public ResponseResult deleteExam(@PathVariable String id){

        return examService.deleteExamById(id);
    }




}
