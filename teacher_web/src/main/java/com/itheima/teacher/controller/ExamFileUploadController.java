package com.itheima.teacher.controller;

import com.alibaba.dubbo.config.annotation.Reference;

import com.itheima.teacher.pojo.Exam;
import com.itheima.teacher.result.PageResult;
import com.itheima.teacher.result.ResponseResult;
import com.itheima.teacher.service.ExamService;
import com.itheima.teacher.utils.ExcelUploadUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/examFile")
public class ExamFileUploadController {



    @Reference
    private ExamService examService;

    @RequestMapping("/fileUpload")
    public ResponseResult fileUpload(MultipartFile file){
        List<Object> objects = ExcelUploadUtils.ExcelForList(file, Exam.class, true);

        examService.saveExamList(objects);

        return null;
    }

    @RequestMapping("/downLoadExam")
    public void download(HttpServletRequest request , HttpServletResponse response){

        List<String> headName = new ArrayList<String>();
        headName.add("姓名");
        headName.add("等级");
        headName.add("描述");
        headName.add("所属班级");
        headName.add("所处阶段");
        headName.add("带班讲师");
        headName.add("带班助教");
        headName.add("带班班主任");
        headName.add("第一次考核链接");
        headName.add("第二次考核链接");
        PageResult pageResult = examService.findAll(1, 10);
        List rows = pageResult.getList();
        System.out.println(rows);
        //将查询的数据转换成  List<List<String>> body
        List<List<String>> body =  ExamFileUploadController.parseList(rows);
        System.out.println(body);
        try {
            Workbook wb = ExamFileUploadController.generateExcel("上机考试报表.xlsx" , headName, body ,response);



            //8.下载（在项目中采用工具类的方式）
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static List<List<String>> parseList(List rows) {

        List<List<String>> body = new ArrayList<List<String>>();
        for (int i = 0 ; i < rows.size() ; i ++) {
            List<String> list = new ArrayList<>();
            Exam exam = (Exam) rows.get(i);
            list.add( exam.getStudentId() );
            list.add(exam.getExamScore());
            list.add(exam.getRemarks());
            list.add(exam.getClassId());
            list.add(exam.getDictId());
            list.add(exam.getLecturer());
            list.add(exam.getAssistant());
            list.add(exam.getClassId());
            list.add(exam.getScoreResultUrl1());
            list.add(exam.getScoreResultUrl2());
            body.add(list);
        }
        return body;
    }

    public static Workbook generateExcel(String sheetName, List<String> header, List<List<String>> body, HttpServletResponse response) {
        // 新建excel报表
        Workbook excel = new HSSFWorkbook();
        // 添加一个sheet
        Sheet hssfSheet = excel.createSheet(sheetName);
        // 往excel表格创建一行，excel的行号是从0开始的
        // 设置表头
        Row firstRow = hssfSheet.createRow(0);
        for (int columnNum = 0; columnNum < header.size(); columnNum++) {
            // 创建单元格
            Cell hssfCell = firstRow.createCell(columnNum);
            // 设置单元格的值
            hssfCell.setCellValue(header.size() < columnNum ? "-" : header.get(columnNum));
        }
        // 手动设置列宽。第一个参数表示要为第几列设；，第二个参数表示列的宽度，n为列高的像素数。
        for (int i = 0; i < body.size() + 7; i++) {
            hssfSheet.setColumnWidth((short) i, (short) (28 * 200));
        }

        // 设置主体数据
        for (int rowNum = 0; rowNum < body.size(); rowNum++) {
            // 往excel表格创建一行，excel的行号是从0开始的
            Row hssfRow = hssfSheet.createRow(rowNum + 1);
            List<String> data = body.get(rowNum);
            for (int columnNum = 0; columnNum < data.size(); columnNum++) {
                // 创建单元格
                Cell hssfCell = hssfRow.createCell(columnNum);
                // 设置单元格的值
                hssfCell.setCellValue(data.size() < columnNum ? "-" : data.get(columnNum));
            }
        }
        try {
            String fileName = "上机考试";
            response.setContentType("application/vnd.ms-excel;charset=utf-8");
            response.setHeader("Content-Disposition", "attachment;filename="+ new String((fileName + ".xls").getBytes(), "iso-8859-1"));

            excel.write(response.getOutputStream());
            response.getOutputStream().close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return excel;
    }


}
