package com.itheima.teacher.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.teacher.pojo.Campus;
import com.itheima.teacher.result.PageResult;
import com.itheima.teacher.result.ResponseResult;
import com.itheima.teacher.service.CampusService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/campus")
public class CampusController {

    @Reference
    private CampusService campusService;

    @GetMapping("/find_all")
    public List<Campus> findAll() {
        return campusService.findAll();
    }

    @GetMapping("/find_page")
    public PageResult findByPage(Integer pageNum, Integer pageSize) {
        return campusService.findByPage(pageNum, pageSize);
    }

    // 添加
    @PostMapping("/insert")
    public ResponseResult insert(@RequestBody Campus campus) {
        return campusService.insert(campus);
    }

    // 修改
    @PutMapping("/update")
    public ResponseResult update(@RequestBody Campus campus) {
        return campusService.update(campus);
    }

    // 删除
    @DeleteMapping("/delete/{id}")
    public ResponseResult delete(@PathVariable Integer id) {
        return campusService.delete(id);
    }

    // 查询一个
    @GetMapping("/find_one/{id}")
    public Campus findOne(@PathVariable Integer id) {
        return campusService.findOne(id);
    }

    // 批量删除
    @DeleteMapping("/dels")
    public ResponseResult dels(Integer [] ids){
        return campusService.dels(ids);
    }

}
