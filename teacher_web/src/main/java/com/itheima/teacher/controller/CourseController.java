package com.itheima.teacher.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.teacher.pojo.Course;
import com.itheima.teacher.result.PageResult;
import com.itheima.teacher.result.ResponseResult;
import com.itheima.teacher.service.CourseService;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/course")
public class CourseController {

    @Reference
    private CourseService courseService;

    @GetMapping("/find_page")
    public PageResult findByPage(Integer pageNum, Integer pageSize) {
        return courseService.findByPage(pageNum, pageSize);
    }

    // 添加
    @PostMapping("/insert")
    public ResponseResult insert(@RequestBody Course course) {
        String id = UUID.randomUUID().toString();
        course.setCourseId(id);
        return courseService.insert(course);
    }

   // 修改
    @PutMapping("/update")
    public ResponseResult update(@RequestBody Course course) {
        return courseService.update(course);
    }

   // 删除
    @DeleteMapping("/delete/{id}")
    public ResponseResult delete(@PathVariable String id) {
        return courseService.delete(id);
    }

    // 查询一个
    @GetMapping("/find_one/{id}")
    public Course findOne(@PathVariable String id) {
        return courseService.findOne(id);
    }

    /*// 批量删除
    @DeleteMapping("/dels")
    public ResponseResult dels(Integer [] ids){
        return courseService.dels(ids);
    }
*/
}
