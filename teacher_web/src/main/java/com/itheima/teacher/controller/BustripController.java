package com.itheima.teacher.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.teacher.pojo.Bustrip;
import com.itheima.teacher.result.PageResult;
import com.itheima.teacher.result.ResponseResult;
import com.itheima.teacher.service.BustripService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/bustrip")
public class BustripController {

    @Reference
    private BustripService bustripService;

    @GetMapping("/find_all")
    public List<Bustrip> findAll() {
        return bustripService.findAll();
    }

    @GetMapping("/find_page")
    public PageResult findByPage(Integer pageNum, Integer pageSize) {
        return bustripService.findByPage(pageNum, pageSize);
    }

    // 添加
    @PostMapping("/insert")
    public ResponseResult insert(@RequestBody Bustrip bustrip) {
        return bustripService.insert(bustrip);
    }

    // 修改
    @PutMapping("/update")
    public ResponseResult update(@RequestBody Bustrip bustrip) {
        return bustripService.update(bustrip);
    }

    // 删除
    @DeleteMapping("/delete/{id}")
    public ResponseResult delete(@PathVariable Integer id) {
        return bustripService.delete(id);
    }

    // 查询一个
    @GetMapping("/find_one/{id}")
    public Bustrip findOne(@PathVariable Integer id) {
        return bustripService.findOne(id);
    }

    // 批量删除
    @DeleteMapping("/dels")
    public ResponseResult dels(Integer [] ids){
        return bustripService.dels(ids);
    }

}
