package com.itheima.teacher.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.teacher.pojo.TeachingScope;
import com.itheima.teacher.result.PageResult;
import com.itheima.teacher.result.ResponseResult;
import com.itheima.teacher.service.TeachingScopeService;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/teachingScope")
public class TeachingScopeController {

    @Reference
    private TeachingScopeService service;

    @GetMapping("/find_page")
    public PageResult findByPage(Integer pageNum, Integer pageSize) {
        return service.findByPage(pageNum, pageSize);
    }

    // 添加
    @PostMapping("/insert")
    public ResponseResult insert(@RequestBody TeachingScope teachingScope) {
        String id = UUID.randomUUID().toString();
        teachingScope.setTeachingScopeId(id);
        return service.insert(teachingScope);
    }

   // 修改
    @PutMapping("/update")
    public ResponseResult update(@RequestBody TeachingScope teachingScope) {
        return service.update(teachingScope);
    }

   // 删除
    @DeleteMapping("/delete/{id}")
    public ResponseResult delete(@PathVariable String id) {
        return service.delete(id);
    }

    // 查询一个
    @GetMapping("/find_one/{id}")
    public TeachingScope findOne(@PathVariable String id) {
        return service.findOne(id);
    }

    /*// 批量删除
    @DeleteMapping("/dels")
    public ResponseResult dels(Integer [] ids){
        return courseService.dels(ids);
    }
*/
}
