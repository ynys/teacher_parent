package com.itheima.teacher.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.teacher.pojo.Performance;
import com.itheima.teacher.result.PageResult;
import com.itheima.teacher.service.PerformanceService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

// 123
@RestController
@RequestMapping("/performance")
public class PerformanceController {

    @Reference
    PerformanceService performanceService;

    @GetMapping("/find_page")
    public PageResult findByPage(Integer pageNum, Integer pageSize) {
        return performanceService.findByPage(pageNum, pageSize);
    }

    @GetMapping("/find_one/{id}")
    public Performance findOne(@PathVariable String id) {
        return performanceService.findOne(id);
    }

}
