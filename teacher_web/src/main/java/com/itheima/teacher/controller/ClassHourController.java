package com.itheima.teacher.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.teacher.pojo.ClassHour;
import com.itheima.teacher.pojo.Course;
import com.itheima.teacher.result.PageResult;
import com.itheima.teacher.result.ResponseResult;
import com.itheima.teacher.service.ClassHourService;
import com.itheima.teacher.service.CourseService;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/classHour")
public class ClassHourController {

    @Reference
    private ClassHourService service;

    @GetMapping("/find_page")
    public PageResult findByPage(Integer pageNum, Integer pageSize) {
        return service.findByPage(pageNum, pageSize);
    }

    // 添加
    @PostMapping("/insert")
    public ResponseResult insert(@RequestBody ClassHour classHour) {
        String id = UUID.randomUUID().toString();
        classHour.setClassHourId(id);
        classHour.setTotalHour(classHour.getDays1()*4+classHour.getDays2()*8+classHour.getDays3()*6);
        return service.insert(classHour);
    }

   // 修改
    @PutMapping("/update")
    public ResponseResult update(@RequestBody ClassHour classHour) {
        classHour.setTotalHour(classHour.getDays1()*4+classHour.getDays2()*8+classHour.getDays3()*6);
        return service.update(classHour);
    }

   // 删除
    @DeleteMapping("/delete/{id}")
    public ResponseResult delete(@PathVariable String id) {
        return service.delete(id);
    }

    // 查询一个
    @GetMapping("/find_one/{id}")
    public ClassHour findOne(@PathVariable String id) {
        return service.findOne(id);
    }

    /*// 批量删除
    @DeleteMapping("/dels")
    public ResponseResult dels(Integer [] ids){
        return courseService.dels(ids);
    }
*/
}
