package com.itheima.teacher.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.teacher.pojo.ClassRating;
import com.itheima.teacher.result.PageResult;
import com.itheima.teacher.result.ResponseResult;
import com.itheima.teacher.service.ClassRatingService;
import org.springframework.web.bind.annotation.*;

/**
 * 老师评分
 */
@RestController
@RequestMapping("/class_rating/")
public class ClassRatingController {

    @Reference
    private ClassRatingService classRatingService;

    @GetMapping("/find_page")
    public PageResult findByPage(Integer pageNum, Integer pageSize) {

        return classRatingService.findByPage(pageNum, pageSize);
    }

    // 添加
    @PostMapping("/insert")
    public ResponseResult insert(@RequestBody ClassRating classRating) {
        return classRatingService.insert(classRating);
    }

    // 修改
    @PutMapping("/update")
    public ResponseResult update(@RequestBody ClassRating classRating) {
        return classRatingService.update(classRating);
    }

    // 删除
    @DeleteMapping("/delete/{id}")
    public ResponseResult delete(@PathVariable String id) {
        return classRatingService.delete(id);
    }

    // 查询一个
    @GetMapping("/find_one/{id}")
    public ClassRating findOne(@PathVariable String id) {
        return classRatingService.findOne(id);
    }

    // 批量删除
    @DeleteMapping("/dels")
    public ResponseResult dels(String[] ids) {
        return classRatingService.dels(ids);
    }
}
