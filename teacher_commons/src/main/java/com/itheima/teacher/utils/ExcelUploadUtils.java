package com.itheima.teacher.utils;

import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.BeanUtils;
import org.springframework.web.multipart.MultipartFile;

import java.beans.PropertyDescriptor;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class ExcelUploadUtils {
    public static List<Object> ExcelForList(MultipartFile file, Class<?> beanclazz, Boolean titleExist) {
        List<Object> list = new ArrayList<Object>();
        try {
            // IO流读取文件
            InputStream input = file.getInputStream();
            // 创建文档
            Workbook wb = WorkbookFactory.create(input);

            // 得到第一张工作表
            Sheet sheet = wb.getSheetAt(0);
            int i;
            if (titleExist) {
                i = 1;
            } else {
                i = 0;
            }

            Object tempObj = beanclazz.newInstance();//设置第一行的临时对象
            // 行的遍历
            for (; i <= sheet.getLastRowNum(); i++) {

                // 得到行
                Row row = sheet.getRow(i);
                // 单元格的遍历
                // 实例化对象
                Object object = beanclazz.newInstance();

                if(i>1){//将第一行的数据赋值给其他行
                    BeanUtils.copyProperties(tempObj , object);
                }

                Field[] fields = beanclazz.getDeclaredFields();
                int j = 0;

                for (Field field : fields) {
                    if(i>1 && j>3){//第三列以后继续循环
                        continue;
                    }
                    String fieldName = field.getName();
                    //获得属性构造器
                    PropertyDescriptor pd = new PropertyDescriptor(fieldName, beanclazz);
                    //获得set方法
                    Method getMethod = pd.getWriteMethod();
                    Cell cell = row.getCell(j++);
                    try{
                        int type = cell.getCellType();

                        if (type == cell.CELL_TYPE_BOOLEAN) {
                            // 返回布尔类型的值
                            boolean value = cell.getBooleanCellValue();
                            getMethod.invoke(object, value);
                        } else if (type == cell.CELL_TYPE_NUMERIC) {
                            // 返回数值类型的值
                            Double d = cell.getNumericCellValue();
                            int value = d.intValue();
                            getMethod.invoke(object, new Integer(value));
                        } else if (type == cell.CELL_TYPE_STRING) {
                            // 返回字符串类型的值
                            String value = cell.getStringCellValue();
                            getMethod.invoke(object, new String(value));
                        }

                    }catch (Exception e) {
                        System.out.println("异常");
                    }
                }
                if(i>1){
                    BeanUtils.copyProperties(object , tempObj);//将数据赋值给临时对象
                }
                list.add(object);//将数据添加到集合中
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }


}
